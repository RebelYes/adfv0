# base image
FROM node:12.2.0-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY ["./Adfluence Frontend/.", "."]
RUN npm install
RUN npm install @vue/cli@3.7.0 -g
RUN npm rebuild node-sass
RUN npm i nohup -g
WORKDIR /app/api
COPY ./loopback/locations/. .

RUN npm install -g

RUN npm start

WORKDIR /app/src

# start app
EXPOSE 3000
CMD npm run serve
