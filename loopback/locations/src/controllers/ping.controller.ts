import {Request, RestBindings, get, ResponseObject,param} from '@loopback/rest';
import {inject} from '@loopback/context';

/**
 * OpenAPI response for ping()
 */
const PING_RESPONSE: ResponseObject = {
  description: 'Ping Response',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        properties: {
          greeting: {type: 'string'},
          date: {type: 'string'},
          url: {type: 'string'},
          headers: {
            type: 'object',
            properties: {
              'Content-Type': {type: 'string'},
            },
            additionalProperties: true,
          },
        },
      },
    },
  },
};


var fs = require("fs");
console.log("\n *START* \n");
var content = fs.readFileSync("usaCities.js");
var jsonContent = JSON.parse(content);
var aps = fs.readFileSync("airport_codes.js");
var airports = JSON.parse(aps);
console.log(jsonContent)
/**
 * A simple controller to bounce back http requests
 */
export class PingController {
  constructor(@inject(RestBindings.Http.REQUEST) private req: Request) {}

  // Map to `GET /ping`
  @get('/ping', {
    responses: {
      '200': PING_RESPONSE,
    },
  })
  ping(): object {
    // Reply with a greeting, the current time, the url, and request headers

    return {
      greeting: 'Hello from LoopBack',
      date: new Date(),
      url: this.req.url,
      headers: Object.assign({}, this.req.headers),
    };
  }
    // Map to `GET /ping`
    @get('/locations/{payload}')
    locations(@param.path.string('payload') str: string): object {
        // Reply with a greeting, the current time, the url, and request headers
        
        let cities = jsonContent.map((x: any) => x.city)
        //console.log(cities)
        //const regex = new RegExp('/^' + str + '/', 'gi');
        var re = new RegExp('^'+str, 'i');
        const matchedCities = cities.filter((city: any) => { return re.test(city)});
        return matchedCities
    }
    @get('/airports/{payload}')
    return_airports(@param.path.string('payload') str: string): object {
        // Reply with a greeting, the current time, the url, and request headers

        let airport_codes = Object.keys(airports)
        let airport_names = Object.entries(airports)
        
        //console.log(cities)
        //const regex = new RegExp('/^' + str + '/', 'gi');
        let ar:any = []
        airport_codes.forEach((x, index) => {ar.push({ name: airport_names[index][1], code:x}) })
        var re = new RegExp('^' + str, 'i');
        console.log(ar)
        const matchedAirports = ar.filter((ap: any) => { return re.test(ap.name) || re.test(ap.code) });
        return matchedAirports
    }
}
